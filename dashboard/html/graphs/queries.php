<?php
/**
 * Created by PhpStorm.
 * User: aritoru
 * Date: 1/7/15
 * Time: 4:13 PM
 */




function getEventTable(){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    $total=getTotalUser();

        // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select distinct shop,(select total from orange.totalResultTable where a.shop=shop and tipo='Geofence') as Geofence,(select total from orange.totalResultTable where a.shop=shop and tipo='Tienda') as Tienda,(select total from orange.totalResultTable where a.shop=shop and tipo='Participa') as Participa,(select total from orange.totalResultTable where a.shop=shop and tipo='Premio') as Premio,(select total from orange.totalResultTable where a.shop=shop and tipo='Total') as Total  from orange.totalResultTable as a";
    $result = $conn->query($sql);
    $data = array();
    $total = array();

    if ($result->num_rows > 0) {
        // output data of each row
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            if ($row['shop']!='TOTAL'){
            $data[]=array('Shop'=>$row['shop'],'Geofence'=>$row['Geofence'],'Tienda'=>$row['Tienda'],
                'RatioGT'=>sprintf('%0.1f', ($row['Tienda']/$row['Geofence'])*100).'%',
                'Participa'=>$row['Participa'],
                'RatioPT'=>sprintf('%0.1f', ($row['Participa']/$row['Tienda'])*100).'%',
                 'Premio'=>$row['Premio'],
                 'RatioPP'=>sprintf('%0.1f', ($row['Premio']/$row['Participa'])*100).'%',
                 'Total'=>$row['Total'] , 
                 'RatioTot'=>sprintf('%0.1f', ( ($row['Premio']+$row['Participa']+$row['Tienda'])
                    /($row['Geofence']+$row['Tienda']+$row['Participa']) )*100).'%'      
                 );
            } else {
                $total[]=array('Geofence'=>$row['Geofence'],'Tienda'=>$row['Tienda'],
                'RatioGT'=>sprintf('%0.1f', ($row['Tienda']/$row['Geofence'])*100).'%',
                'Participa'=>$row['Participa'],
                'RatioPT'=>sprintf('%0.1f', ($row['Participa']/$row['Tienda'])*100).'%',
                 'Premio'=>$row['Premio'],
                 'RatioPP'=>sprintf('%0.1f', ($row['Premio']/$row['Participa'])*100).'%',
                 'Total'=>$row['Total'] , 
                 'RatioTot'=>sprintf('%0.1f', ( ($row['Premio']+$row['Participa']+$row['Tienda'])
                    /($row['Geofence']+$row['Tienda']+$row['Participa']) )*100).'%'      
                 );
            }
            $i = $i + 1;
        }
    }

    $conn->close();


    $columns=array_keys($data[0]);
    // print_r($columns);
    $colorColBack=array('#0e62c7','#3498db','#3498db','#0e62c7','#3498db','#0e62c7','#3498db','#0e62c7','#3498db','#ff0000');
    $colorColText=array('#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff');
    $colorRowBack=array('#ffffff','#ffffff','#ffffff','#0000ff','#ffffff','#0000ff','#ffffff','#0000ff','#0000ff','#0000ff');
    $colorRowText=array('#000000','#000000','#000000','#0e62c7','#000000','#0e62c7','#000000','#ff0000','#000000','#000000');
    return array('data'=>$data,'total'=>$total,
        'columns'=>$columns,
        'colorCol'=>array('back'=>$colorColBack,'text'=>$colorColText),
        'colorRow'=>array('back'=>$colorRowBack,'text'=>$colorRowText),
        'icon'=>'linecons-shop');

}

function getEventDailyTable($filter){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    $total=getTotalUser();

        // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select date(concat(year,'-',month,'-',day)) as date,Geofence,Tienda,Participa,Premio,Total from orange.totalByDay where shop='".$filter."' order by date(concat(year,'-',month,'-',day))";
    $result = $conn->query($sql);
    $data = array();
    $total = array();

    if ($result->num_rows > 0) {
        // output data of each row
        $i = 0;
        while ($row = $result->fetch_assoc()) {

            $data[]=array('date'=>$row['date'],
                'Geofence'=>$row['Geofence'],
                'Tienda'=>$row['Tienda'],
                'Participa'=>$row['Participa'],
                'Premio'=>$row['Premio'],
                 'Total'=>$row['Total']      
                 );

            $i = $i + 1;
        }
    }

    $sql2="select * from orange.Resumentotal where shop='".$filter."'";
    $result2 = $conn->query($sql2);

    if ($result2->num_rows > 0) {
        // output data of each row
        $i = 0;
        while ($row = $result->fetch_assoc()) {

            $total[0]=array('TOTAL'=>$row['TOTAL'],
                'Geofence'=>$row['Geofence'],
                'Tienda'=>$row['Tienda'],
                'Participa'=>$row['Participa'],
                'Premio'=>$row['Premio'],
                 'Total'=>$row['Total']      
                 );

            $i = $i + 1;
        }
    }

    $conn->close();


    $columns=array_keys($data[0]);
    // print_r($columns);
    $colorColBack=array('#0e62c7','#0e62c7','#0e62c7','#0e62c7','#0e62c7','#ff0000');
    $colorColText=array('#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff');
    $colorRowBack=array('#ffffff','#ffffff','#ffffff','#0000ff','#ffffff','#0000ff');
    $colorRowText=array('#000000','#000000','#000000','#000000','#000000','#000000');
    return array('data'=>$data,'total'=>$total,
        'columns'=>$columns,
        'colorCol'=>array('back'=>$colorColBack,'text'=>$colorColText),
        'colorRow'=>array('back'=>$colorRowBack,'text'=>$colorRowText),
        'icon'=>'linecons-calendar');

}



function getTrafficTable(){
    $data=array();
    $data[]=array('Shop'=>'Gavia','Traffic'=>162835,'Passer By'=>11852,'Atraction rate'=>'7.3%','Visitors'=>1559,'Capture rate'=>'13.2%','New visitors'=>'87.34%','Average duration visit'=>'17.66´');
    $data[]=array('Shop'=>'Delicias','Traffic'=>'No data','Passer By'=>'No data','Atraction rate'=>'No data','Visitors'=>'No data','Capture rate'=>'No data','New visitors'=>'No data','Average duration visit'=>'No data');
    $data[]=array('Shop'=>'Goya','Traffic'=>146622,'Passer By'=>7918,'Atraction rate'=>'5.4%','Visitors'=>1841,'Capture rate'=>'23.3%','New visitors'=>'83.08%','Average duration visit'=>'17.95´');
    $data[]=array('Shop'=>'Gran Vía','Traffic'=>703270,'Passer By'=>39950,'Atraction rate'=>'5.7%','Visitors'=>2520,'Capture rate'=>'6.3%','New visitors'=>'89.43%','Average duration visit'=>'15.74´');
    $columns=array_keys($data[0]);
    // print_r($columns);
    $colorColBack=array('#3498db','#3498db','#3498db','#0e62c7','#3498db','#0e62c7','#3498db','#ff0000');
    $colorColText=array('#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff');
    $colorRowBack=array('#ffffff','#ffffff','#ffffff','#0000ff','#ffffff','#0000ff','#ffffff','#0000ff');
    $colorRowText=array('#000000','#000000','#000000','#0e62c7','#000000','#0e62c7','#000000','#ff0000');
    $total=array();
    $total[]=array('Traffic'=>1012727,'Passer By'=>59720,'Atraction rate'=>'5.9%','Visitors'=>5920,'Capture rate'=>'9.9%','New visitors'=>'86.82%','Average duration visit'=>'17.12´');
    return array('data'=>$data,'total'=>$total,
        'columns'=>$columns,
        'colorCol'=>array('back'=>$colorColBack,'text'=>$colorColText),
        'colorRow'=>array('back'=>$colorRowBack,'text'=>$colorRowText),
        'icon'=>'linecons-shop');

}


function getAverageTime(){
    return array(array('Tienda'=>'Gavia','Time'=>17.66),array('Tienda'=>'Goya','Time'=>17.95),array('Tienda'=>'Gran Via','Time'=>15.74));
}

function getDonutVisitorDetail(){
 return array('title'=>'Visitors details',
    'data'=>array( array('Shop'=>'Customer','Percent2'=>'97.70'),
                    array('Shop'=>'Machines','Percent2'=>'1.3'),
                    array('Shop'=>'Staff','Percent2'=>'1.0') 
                )
    );
 
}


function getDonutZipCode(){


 return array('title'=>'Users by ZIP Code',
    'data'=>array( array('zone'=>'Zona Provincia Madrid','Percent6'=>sprintf('%0.3f',( (43)/98)*100 )),
                   array('zone'=>'Zona Madrid Este','Percent6'=>sprintf('%0.3f',( (7)/98)*100 )),
                   array('zone'=>'Zona Madrid Norte','Percent6'=>sprintf('%0.3f',( (14)/98)*100 )),
                   array('zone'=>'Zona Madrid Oeste','Percent6'=>sprintf('%0.3f',( (9)/98)*100 )),
                   array('zone'=>'Zona Madrid Sur','Percent6'=>sprintf('%0.3f',( (25)/98)*100 ))
                )
    );
 
}

function getDonutAverageTime(){


 return array('title'=>'Visiting times for average duration',
    'data'=>array( array('time'=>'0-6 min.','Percent10'=>14),
                   array('time'=>'7-10 min.','Percent10'=>16),
                   array('time'=>'11-18 min.','Percent10'=>32),
                   array('time'=>'19-30 min.','Percent10'=>19),
                   array('time'=>'>30 min.','Percent10'=>19),
                )
    );
 
}





function getDonutSO(){


 return array('title'=>'Users by device',
    'data'=>array( array('device'=>'Android','Percent7'=>sprintf('%0.3f',( (39)/98)*100 )),
                   array('device'=>'iOS','Percent7'=>sprintf('%0.3f',( (59)/98)*100 ))
                )
    );
 
}

function getDonutMMO(){


 return array('title'=>'Users by Mobile Network Operator',
    'data'=>array( array('mmo'=>'Movistar','Percent8'=>sprintf('%0.3f',( (9)/98)*100 )),
        array('mmo'=>'Orange','Percent8'=>sprintf('%0.3f',( (74)/98)*100 )),
        array('mmo'=>'Other','Percent8'=>sprintf('%0.3f',( (10)/98)*100 )),
        array('mmo'=>'Vodafone','Percent8'=>sprintf('%0.3f',( (5)/98)*100 ))
                )
    );
 
}



function getDonutActiveUser(){
 return array('title'=>'Active',
    'data'=>array( array('Shop'=>'Active','Percent4'=>'9.65'),
                    array('Shop'=>'No Active ','Percent4'=>'90.35')
                )
    );
 
}

function getDonutDataForUserAnonimus(){
 return array('title'=>'Anonimus users type',
    'data'=>array( array('Shop'=>'Traffic','Percent3'=>'93.91'),
                    array('Shop'=>'Passer by','Percent3'=>'5.53'),
                    array('Shop'=>'Visitor','Percent3'=>'0.56') 
                )
    );
 
}

function getDonutDataForAtractionRate(){
 return array('title'=>'Atraction Rate by Shop',
    'data'=>array( array('Shop'=>'Gavia','Attraction'=>'7.3'),
                    array('Shop'=>'Goya','Attraction'=>'5.4'),
                    array('Shop'=>'G.Vía','Attraction'=>'5.7') 
                )
    );
 
}


function getDonutDataForCaptureRate(){
 return array('title'=>'Capture Rate by Shop',
    'data'=>array( array('Shop'=>'Gavia','Capture'=>'13.2'),
                    array('Shop'=>'Goya','Capture'=>'23.3'),
                    array('Shop'=>'G.Vía','Capture'=>'6.3') 
                )
    );
 
}

function getVisitorInfo(){

    $res[0]=array('data'=>['6049','-','100%'],
            'color'=>'#0099dd',
            'text'=>['Regular visitors','Rate','Regular visitors Percent'],
            'icon'=>['linecons-mobile']
            );

    $res[1]=array('data'=>[(5920),sprintf('%0.3f', ( (5920)/(6049))*100 ).'%',sprintf('%0.3f',( (5920)/6049)*100 ).'%'],
            'color'=>'#68B828',
            'text'=>['Visitor','PERCENT VISITOR','PERCENT VISITOR'],
            'icon'=>['linecons-user']
            );


    $res[2]=array('data'=>[72,sprintf('%0.3f', ( (72)/(6049))*100 ).'%',sprintf('%0.3f',( (72)/5920)*100 ).'%'],
            'color'=>'#f3c700',
            'text'=>['Machines','Rate','Machines Percent'],
            'icon'=>['linecons-desktop']
            );

    $res[3]=array('data'=>['57',sprintf('%0.3f', ( (57)/(6049))*100 ).'%',sprintf('%0.3f',( (57)/5920)*100 ).'%'],
            'color'=>'#dd2a00',
            'text'=>['Staff','Rate','User Percent'],
            'icon'=>['linecons-mobile']
            );

    return $res;
}




function getNamedUsers(){


    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

        // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select (select count(distinct hash) from orange.usuarios) as totalUser,(select count(distinct hash) from orange.usuarios where fechaCreacion>='20141222') as UserApp,(select count(distinct user) from orange.resultadoFinalUser) as ActiveUser";
    $result = $conn->query($sql);
    $res;

    if ($result->num_rows > 0) {
        // output data of each row
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $res[0] = array('data'=>[$row['totalUser'],'-','100%'],
            'color'=>'#68B828',
            'text'=>['Users Total','Rate','Users Total Percent'],
            'icon'=>['linecons-user']
                );

            $res[1] = array('data'=>[$row['UserApp'],
                sprintf('%0.3f', ( ($row['UserApp'])/($row['totalUser']))*100 ).'%',
                sprintf('%0.3f', ( ($row['UserApp'])/($row['totalUser'])*100 ) ).'%'
                    ],
            'color'=>'#f50000',
            'text'=>['Users App','Rate','Users App Percent'],
            'icon'=>['linecons-database']
                );

            $res[2] = array('data'=>[$row['ActiveUser'],
                sprintf('%0.3f', ( ($row['ActiveUser'])/($row['UserApp']))*100 ).'%',
                sprintf('%0.3f', ( ($row['ActiveUser'])/($row['totalUser'])*100 ) ).'%'],
            'color'=>'#f3c700',
            'text'=>['Active users','Rate','Active users Percent'],
            'icon'=>['linecons-mobile']
                );
            $i = $i + 1;
        }
    }
    $conn->close();
    return $res;

    $res[0]=array('data'=>['5920','-','100%'],
            'color'=>'#68B828',
            'text'=>['Visitor','Rate','Visitor Percent'],
            'icon'=>['linecons-user']
            );

    $res[1]=array('data'=>[(5791),sprintf('%0.3f', ( (5791)/(5920))*100 ).'%',sprintf('%0.3f',( (5791)/5920)*100 ).'%'],
            'color'=>'#f50000',
            'text'=>['Customer user','PERCENT CUSTOMER','PERCENT VISITOR'],
            'icon'=>['linecons-money']
            );


    $res[2]=array('data'=>[72,sprintf('%0.3f', ( (72)/(5791))*100 ).'%',sprintf('%0.3f',( (72)/5920)*100 ).'%'],
            'color'=>'#f3c700',
            'text'=>['Machines','Rate','Machines Percent'],
            'icon'=>['linecons-desktop']
            );

    $res[3]=array('data'=>['57',sprintf('%0.3f', ( (57)/(5791))*100 ).'%',sprintf('%0.3f',( (57)/5920)*100 ).'%'],
            'color'=>'#dd2a00',
            'text'=>['Staff','Rate','User Percent'],
            'icon'=>['linecons-mobile']
            );

    return $res;
}



function getUserData(){

    $res[0]=array('data'=>['1012727','-',sprintf('%0.2f', (1012727/1012727)*100).'%'],
            'color'=>'#00B19D',
            'text'=>['Trafic','Rate','Traffic Percent'],
            'icon'=>['linecons-user']
            );
    
    $res[1]=array('data'=>['59720',sprintf('%0.3f', (59720/1012727)*100).'%' ,sprintf('%0.3f', (59720/1012727)*100).'%' ],
            'color'=>'#0E62C7',
            'text'=>['Passer-By','Rate','Passer-By Percent'],
            'icon'=>['linecons-user']
            );


    $res[2]=array('data'=>['5920',sprintf('%0.3f', (5920/59720)*100).'%',sprintf('%0.3f', (5920/1012727)*100).'%'],
            'color'=>'#68B828',
            'text'=>['Visitor','Rate','Visitor Percent'],
            'icon'=>['linecons-user']
            );


    $res[3]=array('data'=>['98',sprintf('%0.3f', (98/1559)*100).'%',sprintf('%0.3f', (98/1012727)*100).'%'],
            'color'=>'#D5080F',
            'text'=>['User','Rate','User Percent'],
            'icon'=>['linecons-user']
            );

    return $res;
}




function getTotalUser(){


    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

        // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select count(distinct user) as total from orange.resultadoFinal";
    $result = $conn->query($sql);
    $data=0;

    if ($result->num_rows > 0) {
        // output data of each row
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $data = $row['total'];
            $i = $i + 1;
        }
    }
    $conn->close();
    return $data;
    

}


function getUserCampaignDataForShop($filter){



    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    $total=getTotalUser();

        // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select case  when type='Beacon' or Type='Wifi' then 'Tienda' when type='Position'  then 'Geofence' else type end as tipo ,count(distinct user) as total from orange.resultadoFinal where shop='".$filter."' group by case when type='Beacon' or Type='Wifi' then 'Tienda' else type end  ";
    $result = $conn->query($sql);
    $data = array();

    if ($result->num_rows > 0) {
        // output data of each row
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $data[$row['tipo']] = $row['total'];
            $i = $i + 1;
        }
    }

    $conn->close();




    $res[0]=array('data'=>[$data['Geofence'],'-',sprintf('%0.3f', ($data['Geofence']/$total)*100).'%' ],
            'color'=>'#0E62C7',
            'text'=>['Geofence','Rate','Geofence Percent'],
            'icon'=>['linecons-location']
            );

    $res[1]=array('data'=>[$data['Tienda'],sprintf('%0.3f', ($data['Tienda']/$data['Geofence'])*100).'%' ,sprintf('%0.2f', ($data['Tienda']/$total)*100).'%'],
            'color'=>'#00B19D',
            'text'=>['Tienda','Rate','Tienda Percent'],
            'icon'=>['linecons-shop']
            );
    


    $res[2]=array('data'=>[$data['Participa'],sprintf('%0.3f', ($data['Participa']/$data['Tienda'])*100).'%' ,sprintf('%0.3f', ($data['Participa']/$total)*100).'%'],
            'color'=>'#68B828',
            'text'=>['Participa','Rate','Participa Percent'],
            'icon'=>['linecons-cloud']
            );


    $res[3]=array('data'=>[$data['Premio'],sprintf('%0.3f', ($data['Premio']/$data['Participa'])*100).'%',sprintf('%0.3f', ($data['Premio']/$total)*100).'%'],
            'color'=>'#D5080F',
            'text'=>['Premio','Rate','Premio Percent'],
            'icon'=>['linecons-money']
            );

    return $res;
}


function getUserCampaignData(){



    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    $total=getTotalUser();

        // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select case  when type='Beacon' or Type='Wifi' then 'Tienda' when type='Position'  then 'Geofence' else type end as tipo ,count(distinct user) as total from orange.resultadoFinal  group by case when type='Beacon' or Type='Wifi' then 'Tienda' else type end  ";
    $result = $conn->query($sql);
    $data = array();

    if ($result->num_rows > 0) {
        // output data of each row
        $i = 0;
        while ($row = $result->fetch_assoc()) {
            $data[$row['tipo']] = $row['total'];
            $i = $i + 1;
        }
    }

    $conn->close();




    $res[0]=array('data'=>[$data['Geofence'],'-',sprintf('%0.3f', ($data['Geofence']/$total)*100).'%' ],
            'color'=>'#0E62C7',
            'text'=>['Geofence','Rate','Geofence Percent'],
            'icon'=>['linecons-location']
            );

    $res[1]=array('data'=>[$data['Tienda'],sprintf('%0.3f', ($data['Tienda']/$data['Geofence'])*100).'%' ,sprintf('%0.2f', ($data['Tienda']/$total)*100).'%'],
            'color'=>'#00B19D',
            'text'=>['Tienda','Rate','Tienda Percent'],
            'icon'=>['linecons-shop']
            );
    


    $res[2]=array('data'=>[$data['Participa'],sprintf('%0.3f', ($data['Participa']/$data['Tienda'])*100).'%' ,sprintf('%0.3f', ($data['Participa']/$total)*100).'%'],
            'color'=>'#68B828',
            'text'=>['Participa','Rate','Participa Percent'],
            'icon'=>['linecons-cloud']
            );


    $res[3]=array('data'=>[$data['Premio'],sprintf('%0.3f', ($data['Premio']/$data['Participa'])*100).'%',sprintf('%0.3f', ($data['Premio']/$total)*100).'%'],
            'color'=>'#D5080F',
            'text'=>['Premio','Rate','Premio Percent'],
            'icon'=>['linecons-money']
            );

    return $res;
}


function getTrafficByShop(){
        $data=array(array('Tienda'=>'Gavia','Traffic'=>162835),array('Tienda'=>'Goya','Traffic'=>146622),array('Tienda'=>'Gran vía','Traffic'=>703270));
        $total=1012727;
        $mean=337576;

        return array('data'=>$data,'total'=>$total,'mean'=>$mean);
}

function getPasserByShop(){
        $data=array(array('Tienda'=>'Gavia','PasserBy'=>11852),array('Tienda'=>'Goya','PasserBy'=>7918),array('Tienda'=>'Gran vía','PasserBy'=>39950));
        $total=59720;
        $mean=19907;

        return array('data'=>$data,'total'=>$total,'mean'=>$mean);

}

function getVisitorByShop(){
        $data=array(array('Tienda'=>'Gavia','Visitor'=>1559),array('Tienda'=>'Goya','Visitor'=>1841),array('Tienda'=>'Gran vía','Visitor'=>2520)); 
        $total=5920;
        $mean=1973;

        return array('data'=>$data,'total'=>$total,'mean'=>$mean);

}


function getUserShop(){
    $data=array(array('Tienda'=>'Gavia','User'=>26),array('Tienda'=>'Goya','User'=>41),array('Tienda'=>'Gran vía','User'=>61));
        $total=98;
        $mean=42.66;

        return array('data'=>$data,'total'=>$total,'mean'=>$mean);

}


function getUserbyShop(){

    $res[0]=array('Tienda'=>'Gavia',
            'Traffic'=>162835,
            'PasserBy'=>11852,
            'Visitor'=>1559,
            'User'=>26
            );

    $res[1]=array('Tienda'=>'Goya',
            'Traffic'=>146622,
            'PasserBy'=>7918,
            'Visitor'=>1841,
            'User'=>41
            );

    $res[2]=array('Tienda'=>'Gran Via',
            'Traffic'=>703270,
            'PasserBy'=>39950,
            'Visitor'=>2520,
            'User'=>61
            );

    $res[3]=array('Tienda'=>'Delicias',
            'Traffic'=>0,
            'Passer-By'=>0,
            'Visitor'=>0,
            'User'=>38
            );

    return $res;
}

function printTimes($times,$now){

	$t=array_reverse($times);
	$i=0;
	$reg=0;
	echo '<ul id="pagination-flickr">';

	$ahora= new DateTime($now);
	echo 'You are my army Now=';
	print_r($ahora);

	echo date_diff($ahora,new DateTime(''));

	if ($now=='2015-01-15'){ 
		echo '<li class="previous-off">«Previous</li>';
	} else {
		echo '<li class="previous-on">«Previous</li>';
	}
	while ($reg<7 && count($times)<$i){  // Mientras noseleciones 7 registros y no llege al final

		if ($t[$i]['time']==$now) { // Si estoy posicionado en el registro actual
			echo '<li><a href="?time='.$t[$i]['time'].'">'.$t[$i]['time'].'</a></li>';  // Imprimo el actual
			$i=$i+1; 
			$reg=$reg+1;
		} else {
			if (date_diff($ahora,new DateTime($now))<0){
				echo '<li><a href="?time='.$t[$i]['time'].'">'.$t[$i]['time'].'</a></li>';
			}
		}

	} 

	echo '</ul>';


/*
	echo '
	<ul id="pagination-flickr">
		<li class="previous-off">«Previous</li>
		<li class="active">1</li>
		<li><a href="?page=2">2</a></li>
		<li><a href="?page=3">3</a></li>
		<li><a href="?page=4">4</a></li>
		<li><a href="?page=5">5</a></li>
		<li><a href="?page=6">6</a></li>
		<li><a href="?page=7">7</a></li>
		<li class="next"><a href="?page=8">Next »</a></li>
	</ul> ';
	*/
}

function getTimes(){

	$servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

        // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select distinct date(time) as time from orange.resultadoFinal order by time asc";
    $result = $conn->query($sql);
	$data = array();

    if ($result->num_rows > 0) {
        // output data of each row
		$i = 0;
        while ($row = $result->fetch_assoc()) {
            $data[$i] = $row;
            $i = $i + 1;
        }
    }

    return $data;
    $conn->close();
}

function getEvent($tienda,$now)
{

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($tienda == 'No') {

        $sql = "select hour,Geofence,Tienda,Participa,Premio,Total from orange.totalByHour where shop='Todas' and date(concat(year,'-',month,'-',day))='".$now."' order by hour asc";

    } else {
        $sql = "select hour,Geofence,Tienda,Participa,Premio,Total from orange.totalByHour where shop='".$tienda."' and date(concat(year,'-',month,'-',day))='".$now."' order by hour asc";
    }
    $result = $conn->query($sql);

    $data = array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {
            $data[$i] = $row;
            $i = $i + 1;
        }
    } else {
        // echo "0 results";
    }
    //print json_encode($data);
    return $data;
    $conn->close();
}



///////////////////////////
function getUsersByDay()
{

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }



     $sql = "select * from orange.userForDay";


    $result = $conn->query($sql);

    $data = array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {
            $data[] = array('Date'=>$row['date'],'CC LA GAVIA'=>$row['CC LA GAVIA'],'PSO DELICIAS'=>$row['PSO DELICIAS'],'C GRAN VIA'=>$row['C GRAN VIA'],'C GOYA'=>$row['C GOYA'],'TOTAL'=>$row['TOTAL'],);
            $i = $i + 1;
        }
    } else {
        // echo "0 results";
    }
    //print json_encode($data);

    $columns=array_keys($data[0]);
    // print_r($columns);
    $colorColBack=array('#3498db','#3498db','#3498db','#3498db','#3498db','#0e62c7');
    $colorColText=array('#ffffff','#ffffff','#ffffff','#ffffff','#ffffff','#ffffff');
    $colorRowBack=array('#ffffff','#ffffff','#ffffff','#0000ff','#ffffff','#0000ff');
    $colorRowText=array('#000000','#000000','#000000','#0e62c7','#000000','#0e62c7');
    $total=array();
    $conn->close();
    return array('data'=>$data,'total'=>$total,
        'columns'=>$columns,
        'colorCol'=>array('back'=>$colorColBack,'text'=>$colorColText),
        'colorRow'=>array('back'=>$colorRowBack,'text'=>$colorRowText),
        'icon'=>'linecons-clock');
}



function getUserResume(){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select shop,count(distinct user) as user from orange.resultadoFinal group by shop union all select 'TOTAL',count(distinct user) from orange.resultadoFinal";


    $result = $conn->query($sql);

    $data= array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {
            if ($row['shop']!='TOTAL') {
                $data[]=array('shop'=>$row['shop'],'user'=>$row['user']);
            } else {
                $total=$row['user'];
            }



            $i = $i + 1;
        }
    } else {
        // echo "0 results";
    }
    //print json_encode($data);
    $conn->close();
    return array('data'=>$data,'total'=>$total);
    
}


function getShopResume($filter){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "select tipo,total from orange.totalResultTable where shop='".$filter."' and tipo='Geofence' union all select tipo,total from orange.totalResultTable where shop='".$filter."' and tipo='Tienda' union all select tipo,total from orange.totalResultTable where shop='".$filter."' and tipo='Participa' union all select tipo,total from orange.totalResultTable where shop='".$filter."' and tipo='Premio' union all select tipo,total from orange.totalResultTable where shop='".$filter."' and tipo='Total'";


    $result = $conn->query($sql);

    $data= array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {

            if ($row['tipo']!='Total') {
                $data[]=array('Tipo'=>$row['tipo'],'users'=>$row['total']);
            } else {
                $total=$row['total'];
            }
            $i = $i + 1;
        }
    } else {
        // echo "0 results";
    }
    //print json_encode($data);
    $conn->close();
    return array('data'=>$data,'total'=>$total);
    
}


function getVisitFrecuency()
{

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


    $sql = "select case when numero=9999 then 'Other' else numero end as Visits,usuarios as Users  from orange.frecuenciasVisitas";


    $result = $conn->query($sql);

    $data = array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {
            $data[$i] = $row;
            $i = $i + 1;
        }
    } else {
        echo "0 results";
    }
    //print json_encode($data);
    $conn->close();
    return $data;
    
}



function getAverageTimeForVisit()
{

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }


    $sql = "select case when numero=9999 then 'Other' else numero end as Visits,tiempo as Time  from orange.TiempoMedioVisitas";


    $result = $conn->query($sql);

    $data = array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {
            $data[$i] = $row;
            $i = $i + 1;
        }
    } else {
        echo "0 results";
    }
    //print json_encode($data);
    $conn->close();
    return $data;
    
}


function getTotal($tienda)
{

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($tienda == 'No') {

        $sql = "select concat(year,'/',month,'/',day) as 'Year_Month_Day',Geofence,Tienda,Participa,Premio,Total from orange.totalByDay where shop='Todas' order by date(concat(year,'/',month,'/',day)) desc";

    } else {
        
        $sql = "select concat(year,'/',month,'/',day) as 'Year_Month_Day',Geofence,Tienda,Participa,Premio,Total from orange.totalByDay where shop='".$tienda."' order by date(concat(year,'/',month,'/',day)) desc";
    }

    $result = $conn->query($sql);

    $data = array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {
            $data[$i] = $row;
            $i = $i + 1;
        }
    } else {
        echo "0 results";
    }
    //print json_encode($data);
    $conn->close();
    return $data;
    
}


function getTotalFinal($tienda)
{

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    switch ($tienda) {
        case 'No':
            $sql = "select (select count(distinct user) from orange.resultadoFinal where (type='Position') ) as Geofence, (select count(distinct user) from orange.resultadoFinal where (type='Beacon' or type='Wifi') ) as Tienda,(select count(distinct user) from orange.resultadoFinal where (type='Participa') ) as Participa,(select count(distinct user) from orange.resultadoFinal where (type='Premio') ) as Premio,(select count(distinct user) from orange.resultadoFinal ) as Total";
            break;
        case 'All':
            $sql = "select shop,count(distinct user) as total,(count(distinct user)/(select count(distinct shop,user) from orange.resultadoFinal))*100 as percent from orange.resultadoFinal group by shop";
            break;
        default:
            $sql = "select (select count(distinct user) from orange.resultadoFinal where (type='Position') and shop='".$tienda."' ) as Geofence, (select count(distinct user) from orange.resultadoFinal where (type='Beacon' or type='Wifi') and shop='".$tienda."' ) as Tienda,(select count(distinct user) from orange.resultadoFinal where (type='Participa') and shop='".$tienda."') as Participa,(select count(distinct user) from orange.resultadoFinal where (type='Premio') and shop='".$tienda."') as Premio";
            break;

    }

    $result = $conn->query($sql);

    $data = array();
    $i = 0;
    if ($result->num_rows > 0) {
        // output data of each row

        while ($row = $result->fetch_assoc()) {
            $data[$i] = $row;
            $i = $i + 1;
        }
    } else {
        echo "0 results";
    }

    //print json_encode($data);
    return $data;
    $conn->close();
}





function getTodayNum($field, $data)
{

    foreach ($data as $day) {
        if ($day['Year_Month_Day'] == date("Y/m/d", time())) {
            return $day[$field];
        }
    }


}

function getTotalNum($field, $data)
{

    $total = 0;
    foreach ($data as $day) {
        $total = $total + $day[$field];
    }
    return $total;

}

function getFunnelData($data) {

    $final = array();
    foreach ($data as $key => $value) {
        if ($key != 'Shop' && $key != 'Total') {
            $partial = array($key,$value);
            $final[]=$partial;
        }
    }
    return $final;
}


function getClienByCp($filter){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($filter=='No'){ // Si es el total
        $sql="select CodigoPostal as 'CP',count(distinct user) as User,(count(distinct user)/(select count(distinct user) from orange.resultadoFinalUser))*100 as Total from orange.resultadoFinalUser group by CodigoPostal order by CodigoPostal";
        //echo '</br>'.$sql;
        $result = $conn->query($sql);

        $data = array();
        $i = 0;

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $data[$i] = $row;
                $i = $i + 1;
            }

            $d['Title']='Unique user by Postal Code';
            $d['Data']=$data;

            // print_r($data);
        } 
        } else { // Si es una Sola tienda
        // echo "0 results";
        $sql="select Geofence,Tienda,Participa,Premio from totalfinal where shop='".$filter."'";
        $result = $conn->query($sql);
        $data = array();
        $i = 0;
        $total=0;

            if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            $total+=$row['Geofence']+$row['Tienda']+$row['Participa']+$row['Premio'];
                            $data[0]['Shop'] = 'Geofence';
                            $data[0]['Total'] = $row['Geofence'];
                            $data[0]['Percent1'] = $row['Geofence']/$total*100.00;
                            $data[1]['Shop'] = 'Tienda';
                            $data[1]['Total'] = $row['Tienda'];
                            $data[1]['Percent1'] = $row['Tienda']/$total*100.00;
                            $data[2]['Shop'] = 'Participa';
                            $data[2]['Total'] = $row['Participa'];
                            $data[2]['Percent1'] = $row['Participa']/$total*100.00;
                            $data[3]['Shop'] = 'Premio';
                            $data[3]['Total'] = $row['Premio'];
                            $data[3]['Percent1'] = $row['Premio']/$total*100.00;
                            
                        }
                $d['Title']='Distribution of unique users by event type';
                $d['Data']=$data;

                }

        }
    return $d;
    }

function getDataCircle($filter){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($filter=='No'){ // Si es el total
        $sql="select shop as Shop,count(distinct user) as Total from orange.resultadoFinal group by shop";
        //echo '</br>'.$sql;
        $result = $conn->query($sql);

        $data = array();
        $i = 0;
        $total=0;
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $data[$i] = $row;
                $total+=$row['Total'];
                $i = $i + 1;
            }

            for ($i=0;$i<count($data);$i++){
                $data[$i]['Percent1']=($data[$i]['Total']/$total)*100.00;
            }
            $d['Title']='Unique users by Shop';
            $d['Data']=$data;

            // print_r($data);
        } 
        } else { // Si es una Sola tienda
        // echo "0 results";
        $sql="select Geofence,Tienda,Participa,Premio from totalfinal where shop='".$filter."'";
        $result = $conn->query($sql);
        $data = array();
        $i = 0;
        $total=0;

            if ($result->num_rows > 0) {
                        while ($row = $result->fetch_assoc()) {
                            $total+=$row['Geofence']+$row['Tienda']+$row['Participa']+$row['Premio'];
                            $data[0]['Shop'] = 'Geofence';
                            $data[0]['Total'] = $row['Geofence'];
                            $data[0]['Percent1'] = $row['Geofence']/$total*100.00;
                            $data[1]['Shop'] = 'Tienda';
                            $data[1]['Total'] = $row['Tienda'];
                            $data[1]['Percent1'] = $row['Tienda']/$total*100.00;
                            $data[2]['Shop'] = 'Participa';
                            $data[2]['Total'] = $row['Participa'];
                            $data[2]['Percent1'] = $row['Participa']/$total*100.00;
                            $data[3]['Shop'] = 'Premio';
                            $data[3]['Total'] = $row['Premio'];
                            $data[3]['Percent1'] = $row['Premio']/$total*100.00;
                            
                        }
                $d['Title']='Distribution of unique users by event type';
                $d['Data']=$data;

                }

        }
    return $d;
    }



function getDataCircleForOperator($filter){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($filter=='No'){ // Si es el total
        $sql="select operador,count(distinct user) as usuarios from orange.resultadoFinalUser group by operador";
        //echo '</br>'.$sql;
        $result = $conn->query($sql);

        $data = array();
        $i = 0;
        $total=0;
        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $data[$i] = $row;
                $total+=$row['usuarios'];
                $i = $i + 1;
            }

            for ($i=0;$i<count($data);$i++){
                $data[$i]['Percent2']=($data[$i]['usuarios']/$total)*100.00;
            }

            $d['Title']='Unique users by Mobile Operator';
            $d['Data']=$data;

            // print_r($data);
        } 
        } else { // Si es una Sola tienda
        // echo "0 results";
        $sql="select operador,count(distinct user) as usuarios from orange.resultadoFinalUser where shop='".$filter."' group by operador";
        $result = $conn->query($sql);
        $data = array();
        $i = 0;
        $total=0;

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $data[$i] = $row;
                $total+=$row['usuarios'];
                $i = $i + 1;
            }

            for ($i=0;$i<count($data);$i++){
                $data[$i]['Percent2']=($data[$i]['usuarios']/$total)*100.00;
            }

            $d['Title']='Unique users by Mobile Operator';
            $d['Data']=$data;

            // print_r($data);
        }

        }
    return $d;
    }



    
    function getPositionEvent($filter){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($filter=='No'){ // Si es el total
        $sql="select distinct concat(year,'-',month,'-',day) as 'Year_Month_Day',ifnull(sum(b.user),0) as 'Beacon',ifnull(sum(c.user),0) as 'Wifi',ifnull(sum(d.user),0) as 'Geofence'  from alltimes2 as a left join eventDetection as b on concat(a.year,'-',a.month,'-',a.day)=b.Year_Month_Day and b.type='Beacon' and a.tienda=b.Shop left join eventDetection as c on concat(a.year,'-',a.month,'-',a.day)=c.Year_Month_Day and c.type='Wifi' and a.tienda=c.Shop left join eventDetection as d on concat(a.year,'-',a.month,'-',a.day)=d.Year_Month_Day and d.type='Position' and a.tienda=d.Shop where a.tienda='CC LA GAVIA' or a.tienda='C GRAN VIA' or a.tienda='C GOYA' or a.tienda='PSO DELICIAS' group by concat(year,'-',month,'-',day)";
    } else {
        $sql="select distinct concat(year,'-',month,'-',day) as 'Year_Month_Day',tienda as Shop,ifnull(b.user,0) as 'Beacon',ifnull(c.user,0) as 'Wifi',ifnull(d.user,0) as 'Geofence'  from alltimes2 as a left join eventDetection as b on concat(a.year,'-',a.month,'-',a.day)=b.Year_Month_Day and b.type='Beacon' and a.tienda=b.Shop left join eventDetection as c on concat(a.year,'-',a.month,'-',a.day)=c.Year_Month_Day and c.type='Wifi' and a.tienda=c.Shop left join eventDetection as d on concat(a.year,'-',a.month,'-',a.day)=d.Year_Month_Day and d.type='Position' and a.tienda=d.Shop where a.tienda='".$filter."'";

    }

        $result = $conn->query($sql);

        $data = array();
        $i = 0;
        $tag=array();


        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }

            $d['Tag']=['Beacon','Wifi','Geofence'];
            $d['Data']=$data;

            //print_r($data);
        } 
    return $d;
    }



    function getPromotionEvent($filter){

    $servername = "replica.cstjjfplls2x.eu-west-1.rds.amazonaws.com";
    $username = "admin";
    $password = "gennion03app";
    $dbname = "mogile_db";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    if ($filter=='No'){ // Si es el total
        $sql="select distinct concat(year,'-',month,'-',day) as 'Year_Month_Day',tienda as Shop,ifnull(sum(b.user),0) as 'Participa',ifnull(sum(c.user),0) as 'Premio' from alltimes2 as a left join promotionDetection as b on concat(a.year,'-',a.month,'-',a.day)=b.Year_Month_Day and b.type='CheckPosition' and a.tienda=b.Shop left join promotionDetection as c on concat(a.year,'-',a.month,'-',a.day)=c.Year_Month_Day and c.type='Coupon' and a.tienda=c.Shop where (a.tienda='CC LA GAVIA' or a.tienda= 'C GRAN VIA' or a.tienda = 'C GOYA' or a.tienda ='PSO DELICIAS') group by concat(year,'-',month,'-',day)";
    } else {
        $sql="select distinct concat(year,'-',month,'-',day) as 'Year_Month_Day',tienda as Shop,ifnull(b.user,0) as 'Participa',ifnull(c.user,0) as 'Premio' from alltimes2 as a left join promotionDetection as b on concat(a.year,'-',a.month,'-',a.day)=b.Year_Month_Day and b.type='CheckPosition' and a.tienda=b.Shop left join promotionDetection as c on concat(a.year,'-',a.month,'-',a.day)=c.Year_Month_Day and c.type='Coupon' and a.tienda=c.Shop where a.tienda='".$filter."'";

    }

        $result = $conn->query($sql);

        $data = array();
        $i = 0;
        $tag=array();


        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                $data[] = $row;
            }

            $d['Tag']=['Participa','Premio'];
            $d['Data']=$data;

            //print_r($data);
        } 
    return $d;
    }

