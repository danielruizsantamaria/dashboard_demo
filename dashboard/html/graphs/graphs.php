<?php
/**
 * Created by PhpStorm.
 * User: aritoru
 * Date: 12/22/14
 * Time: 3:40 PM
 */

//include_once('queries.php');

/**
 * @param $data
 * @param $title
 * @param $layer
 * @param $valueField
 * @param $argumentField
 */
function donutGraph($d2,$title, $layer, $valueField, $argumentField)
{


    echo '

        <script type="text/javascript">

                    var dataSource' . $valueField . ' = ' . json_encode($d2, JSON_NUMERIC_CHECK) . ';

                jQuery(document).ready(function ($) {

                   $("#' . $layer . '").dxPieChart({
                    //title: "Gender",
                    dataSource: dataSource' . $valueField . ',
                    palette: "Soft Pastel",
                    legend: {
                        visible: true,
                        horizontalAlignment: "center",
                        verticalAlignment: "bottom"
                    },
                    series: [{
                        smallValuesGrouping: {
                            mode: "topN",
                            topCount: 10
                        },
                        type: "doughnut",
                        argumentField: "' . $argumentField . '",
                        valueField: "' . $valueField . '",
                        label: {
                            visible: true,
                            format: "fixedPoint",
                            customizeText: function (point) {
                                return point.argumentText + ": " + point.valueText + "%";
                            },
                            connector: {
                                visible: true,
                                width: 1
                            }
                        }
                    }]
                });
                });

                $(window).on("xenon.resize", function () {
                    $("#' . $layer . '").data("dxPieChart").render();

                });
        </script>

                <div class="panel panel-default" >
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">                                      
<!--
                            Data:
                            <pre>'.print_r($d2,true).'</pre> 
-->
                        <div id="' . $layer . '"></div>
                    </div>
                </div>


    ';


}


function donutGraph2($d2,$title, $layer, $valueField, $argumentField)
{


    echo '

        <script type="text/javascript">

                    var dataSource' . $valueField . ' = ' . json_encode($d2, JSON_NUMERIC_CHECK) . ';

                jQuery(document).ready(function ($) {

                   $("#' . $layer . '").dxPieChart({
                    //title: "Gender",
                    dataSource: dataSource' . $valueField . ',
                    palette: "Soft Pastel",
                    legend: {
                        visible: true,
                        horizontalAlignment: "center",
                        verticalAlignment: "bottom"
                    },
                    series: [{
                        smallValuesGrouping: {
                            mode: "topN",
                            topCount: 10
                        },
                        type: "doughnut",
                        argumentField: "' . $argumentField . '",
                        valueField: "' . $valueField . '",
                        label: {
                            visible: true,
                            format: "fixedPoint",
                            customizeText: function (point) {
                                return point.argumentText + ": " + point.valueText + "%";
                            },
                            connector: {
                                visible: true,
                                width: 1
                            }
                        }
                    }]
                });
                });

                $(window).on("xenon.resize", function () {
                    $("#' . $layer . '").data("dxPieChart").render();

                });
        </script>

                <div class="panel panel-default" >
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">                                      
<!--
                            Data:
                            <pre>'.print_r($d2,true).'</pre> 
-->
                        <div id="' . $layer . '"></div>
                    </div>
                </div>


    ';


}

/**
 * @param $layer
 * @param $startValue
 * @param $endValue
 * @param $tick
 * @param $text
 * @param $value
 * @param $title
 */

function accelGraph($layer, $startValue, $endValue, $tick, $text, $value, $title)
{

    echo '

                <script type="text/javascript">
                    jQuery(document).ready(function ($) {

                    $("#' . $layer . '").dxCircularGauge({
                        scale: {
                            startValue: ' . $startValue . ', endValue: ' . $endValue . ',
                            majorTick: {tickInterval: ' . $tick . '},
                        },
                        title: {
                            text: "' . $text . '",
                            font: {size: 20}
                        },
                        rangeContainer: {
                            palette: "Soft Pastel",
                            ranges: [
                                {startValue: ' . $startValue . ', endValue: ' . $endValue . '}
                            ]
                        },
                        value: ' . $value . ',
                        subvalues: [' . $value . ']
                    });


                });


                $(window).on("xenon.resize", function () {
                    $("#' . $layer . '").data("dxCircularGauge").render();

                });
                </script>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">

                        <div id="' . $layer . '" style="padding=100px;"></div>
                    </div>
                </div>


    ';

}


function bulletGraph($data, $layer, $startScale, $endScale, $valueField, $conversionField, $nameField, $color, $title)
{

    //echo json_encode($data, JSON_NUMERIC_CHECK);
    //print_r(json_encode($data,JSON_NUMERIC_CHECK));
    //print_r($data,true);

    echo '
       <script type="text/javascript">

                    var dataSource' . $valueField . ' = ' . json_encode($data, JSON_NUMERIC_CHECK) . ';
                    jQuery(document).ready(function ($) {

                        function printFinalTable(piece) {
                            var namePlain = piece.' . $nameField.'

                            $("#' . $layer . '").append("<tr><td>" + piece.' . $nameField . ' + "</td><td>" +  piece.' . $valueField . ' + "</td><td>" + piece.' . $conversionField . ' + "%</td><td><div id=\'' . $layer . '" + namePlain + "-bullet\'></div></td></tr>");
                            var conversion = (piece.' . $conversionField . ' * piece.' . $valueField . ') / 100;
                            $("#' . $layer . '" + namePlain + "-bullet").dxBullet({
                                startScaleValue: ' . $startScale . ',
                                endScaleValue: ' . $endScale . ',
                                target: conversion,

                                value: piece.' . $valueField . ',

                                color: "' . $color . '"
                            });
                        }

                        dataSource' . $valueField . '.map(printFinalTable);


                    });
            </script>

                    <div class="panel panel-default">
            <div class="panel-heading">' . $title . '</div>
            <div class="panel-body">

<!--
                                                Data:
                            <pre>'.print_r($data,true).'</pre>
-->
                <table class="table" id="' . $layer . '-table">
                    <thead>
                    <tr>
                        <th width="20%">' . $nameField . '</th>
                        <th width="20%">' . $valueField . '</th>
                        <th width="20%">' . $conversionField . '</th>
                        <th width="40%">Chart</th>
                    </tr>
                    </thead>
                    <tbody id="' . $layer . '">
                    </tbody>
                </table>
            </div>
        </div>
    ';



}



/**
 * @param $data
 * @param $color
 * @param $title
 * @param $today
 * @param $total
 * @param $layer
 * @param $valueField
 * @param $valueName
 * @param $argumentField
 */

function barGraph($data, $tag, $title,$subtitle, $layer, $valueField, $argumentField)



{
    //print_r($data);
    echo '

            <script type="text/javascript">

                    var dataSource' . $tag . ' = ' . json_encode($data, JSON_NUMERIC_CHECK) . ';
                    jQuery(document).ready(function ($) {

                            $("#' . $layer . '").dxChart({
                            dataSource: dataSource'.$tag;

    $d= 'series: [';
        for($i=0;$i<count($valueField);$i++){
            if ($i>0){$d=$d. ',';}
            $d=$d.'{argumentField:"'.$argumentField.'",valueField:"'.$valueField[$i].'",name:"'.$valueField[$i].'",type:"bar"}';
        }
    $d=$d. '],palette: "Hard Pastel",
                                 title: "'.$subtitle.'",
                                 argumentAxis: { type: "discrete" }

                                                legend: {
                        visible: true,
                        horizontalAlignment: "center",
                        verticalAlignment: "bottom"
                    }

                                 ';

echo $d;


echo '                            });

                    });


                    $(window).on("xenon.resize", function () {
                        $("#' . $layer . '").data("dxChart").render();

                    });
            </script>

                <div class="panel panel-default"  >
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">
<!--
                                                Data:
                            <pre>'.print_r($data,true).'</pre>
-->                         
                        <div id="' . $layer . '"></div>
                    </div>
                </div>

    ';

}





function barGraph2($data, $tag, $title,$subtitle, $layer, $valueField, $argumentField,$type)

{
    echo json_encode($data, JSON_NUMERIC_CHECK);;
    echo '

            <script type="text/javascript">

                    var dataSource' . $tag . ' = ' . json_encode($data, JSON_NUMERIC_CHECK) . ';
                    jQuery(document).ready(function ($) {

                            $("#' . $layer . '").dxChart({
                            dataSource: dataSource'.$tag.',';


    $d= 'series: [';
        for($i=0;$i<count($valueField);$i++){
            if ($i>0){$d=$d. ',';}
            $d=$d.'{argumentField:"'.$argumentField.'",valueField:"'.$valueField[$i].'",name:"'.$valueField[$i].'",type:"'.$type.'"}';
        }


    $d=$d. '],palette: "Hard Pastel",
                                 title: "'.$subtitle.'",
                                 argumentAxis: { type: "discrete" }';

echo $d;


echo '                            });

                    });


                    $(window).on("xenon.resize", function () {
                        $("#' . $layer . '").data("dxChart").render();

                    });
            </script>

                <div class="panel panel-default"  >
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">

                                                Data:
                            <pre>'.print_r($data,true).'</pre>
                       
                        <div id="' . $layer . '"></div>
                    </div>
                </div>

    ';

}












function lineGraph($data, $tag, $title,$subtitle, $layer, $valueField, $argumentField,$type='bar')



{
    //print_r($data);
    echo '

            <script type="text/javascript">

                    var dataSource' . $tag . ' = ' . json_encode($data, JSON_NUMERIC_CHECK) . ';
                    jQuery(document).ready(function ($) {

                            $("#' . $layer . '").dxChart({
                            dataSource: dataSource'.$tag;

    $d= 'series: [';
        for($i=0;$i<count($valueField);$i++){
            if ($i>0){$d=$d. ',';}
            $d=$d.'{argumentField:"'.$argumentField.'",valueField:"'.$valueField[$i].'",name:"'.$valueField[$i].'",type:"'.$type.'"}';
        }
    $d=$d. '],palette: "Hard Pastel",
                                 title: "'.$subtitle.'",
                                 argumentAxis: { type: "discrete" }

                                                legend: {
                        visible: true,
                        horizontalAlignment: "center",
                        verticalAlignment: "bottom"
                    }

                                 ';

echo $d;


echo '                            });

                    });


                    $(window).on("xenon.resize", function () {
                        $("#' . $layer . '").data("dxChart").render();

                    });
            </script>

                <div class="panel panel-default"  >
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">
<!--
                                                Data:
                            <pre>'.print_r($data,true).'</pre>
-->                         
                        <div id="' . $layer . '"></div>
                    </div>
                </div>

    ';

}



/*

function barGraph($data, $color, $title, $layer, $valueField, $argumentField)

{
    //print_r($data);
    echo '

            <script type="text/javascript">

                    var dataSource' . $valueField . ' = ' . json_encode($data, JSON_NUMERIC_CHECK) . ';
                    jQuery(document).ready(function ($) {

                            $("#' . $layer . '").dxChart({
                            dataSource: dataSource' . $valueField . ',
                                 commonSeriesSettings: {
                                    argumentField: "' . $argumentField . '"
                                    },


                                series: [{
                                    argumentField: "' . $argumentField . '",
                                    valueField: "Tienda",
                                    name: "Tienda",
                                    type: "line",

                                },{
                                    argumentField: "' . $argumentField . '",
                                    valueField: "Geofence",
                                    name: "Geofence",
                                    type: "line",

                                },{
                                    argumentField: "' . $argumentField . '",
                                    valueField: "Participa",
                                    name: "Participa",
                                    type: "line",

                                },{
                                    argumentField: "' . $argumentField . '",
                                    valueField: "Premio",
                                    name: "Premio",
                                    type: "line",
  
                                },{
                                    argumentField: "' . $argumentField . '",
                                    valueField: "Total",
                                    name: "Total",
                                    type: "line",
                                }],
                                 palette: "Soft Pastel",
                                 title: "'.$title.'",
                                 argumentAxis: { type: "discrete" }



                            });

                    });


                    $(window).on("xenon.resize", function () {
                        $("#' . $layer . '").data("dxChart").render();

                    });
            </script>

                <div class="panel panel-default"  >
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">

                                                Data:
                            <pre>'.print_r($data,true).'</pre>

                        <div id="' . $layer . '"></div>
                    </div>
                </div>

    ';


}

/*



/**
 * @param $data
 * @param $color
 * @param $title
 * @param $today
 * @param $total
 * @param $layer
 * @param $valueField
 * @param $valueName
 * @param $argumentField
 */

function printLine($name){

echo '  <div ng-if="layoutOptions.pageTitles" title="Panels" description="Panels and their variants"
             class="page-title full-width ng-scope" style="margin-top: 10px;">
            <div class="title-env">
                <h1 class="title ng-binding">'.$name.'</h1>

                <p class="description ng-binding"></p>
            </div>
        </div>';
}

function printLineSub($name){

echo '  <div ng-if="layoutOptions.pageTitles" title="Panels" description="Panels and their variants"
             class="page-title full-width ng-scope" style="margin-top: 10px;">
            <div class="title-env">
                <p class="description ng-binding">'.$name.'</p>
            </div>
        </div>';
}



function printSquareColumn($data){

            echo '<div class="col-sm-3">
                <div class="xe-widget xe-progress-counter xe-progress-counter-info" xe-counter="" data-suffix="" style="background-color: '.$data['color'].';"
                     data-count=".num" data-from="0" data-to="'.$data['data'][0].'" data-duration="4" data-easing="true">
                    <div class="xe-upper">

                        <div class="xe-icon">
                            <i class="'.$data['icon'][0].'"></i>
                        </div>
                        <div class="xe-label">
                            <strong class="num">'.$data['data'][0].'</strong>
                            <span>'.$data['text'][0].'</span>
                        </div>

                    </div>  <!-- End of block --> ';
 if (count($data['data'])>1){
  echo '
                <div class="xe-lower">
                    <div class="border"></div>

                        <span>'.$data['text'][1].'</span>
                        <strong>'.$data['data'][1].'</strong>
                    </div>
                </div>'; }

 if (count($data['data'])>2){
  echo '
                <div class="xe-widget xe-counter xe-counter-blue" xe-counter="" data-count=".num" data-from="0"
                     data-to="'.$data['data'][2].'" data-suffix="" data-duration="3" data-easing="true">
                    <div class="xe-icon">
                        <i class="linecons-user"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">'.$data['data'][2].'</strong>
                        <span>'.$data['text'][2].'</span>
                    </div>
                </div>
            </div>';

    }
}


function printSquareRow($data){

    echo '<div class="row">';

    for ($i=0;$i<count($data);$i++){
        if (isset($data[$i])){
            printSquareColumn($data[$i]);
        } else {
            printSquareColumn($data);
        }

    }
    echo '</div>';
}




function printSquare($data)
{

   // echo count($data);
    
    
$i=0;
while ($i<count($data)) {
    $d=$data[$i];
                


  echo '

        <div class="row">
            <div class="col-sm-3">
                <div class="xe-widget xe-progress-counter xe-progress-counter-info" xe-counter="" data-suffix="" style="background-color: '.$data['color'].';"
                     data-count=".num" data-from="0" data-to="'.$d['data'][0].'" data-duration="2" data-easing="true">
                    
                    <div class="xe-upper"> <!-- ok -->

                        <div class="xe-icon">  <!-- ok -->
                            <i class="'.$d['icon'][0].'"></i>
                        </div> <!-- ok -->
                        <div class="xe-label"> <!-- ok -->
                            <strong class="num">'.$d['data'][0].'</strong>
                            <span>'.$d['text'][0].'</span>
                        </div> <!-- ok -->

                    </div> <!-- ok -->' ;

    if (count($d['data'])>1){
                    
        echo '      <div class="xe-lower"> <!-- ok -->
                            <div class="border"></div> <!-- ok -->

                            <span>'.$d['text'][1].'</span>
                            <strong>'.$d['data'][1].'</strong>
                        </div> <!-- ok -->

                    </div> <!-- ok -->';
            }


if (count($d['data'])>2) {
    // $tot=($data['datos'][$i][1]/$data['total'][0])*100;

    echo '            

                    <div class="xe-widget xe-counter xe-counter-blue" xe-counter="" data-count=".num" data-from="1"
                         data-to="'.$d['data'][2].'" data-suffix="" data-duration="3" data-easing="true"> <!-- ok -->
                        <div class="xe-icon"> <!-- ok -->
                            <i class="linecons-user"></i>
                        </div> <!-- ok -->
                        <div class="xe-label"> <!-- ok -->
                            <strong class="num">'.$d['data'][2].'</strong>
                            <span>'.$d['text'][2].'</span>
                        </div> <!-- ok -->
                    </div> <!-- ok -->';

        }

    echo '
                
            </div><!-- End col-->';
 
    $i=$i+1;
    }

echo '
    </div><!-- End ROW -->';
   
}



//////////////////////////////////////////////////////////////////////////////////

function lineGraph2($id,$dataHistograma,$dataUp,$dataButton
    ,$colorData,$colorAxis,$colorUp,$colorType
    ,$colorButton, $tag1,$tag2,$tag3, $layer, $valueField, $valueName, $argumentField)
{

    //print_r(json_encode($data,JSON_NUMERIC_CHECK));
    echo '

        <script type="text/javascript">

            var dataSource' . $id . ' = ' . json_encode($dataHistograma, JSON_NUMERIC_CHECK) . ';


            jQuery(document).ready(function ($) {

                $("#' . $layer . '").dxChart({
                    dataSource: dataSource' . $id . ',
                    commonPaneSettings: {
                        border: {
                            visible: false,
                            color:"'.$colorData.'"
                        }
                    },

                    commonSeriesSettings: {
                        type: "bar",
                        argumentField: "' . $argumentField . '",
                        border: {
                            color: "' . $colorData . '",
                            width: 1,
                            visible: true
                        },
        hoverMode: "allArgumentPoints",
        selectionMode: "allArgumentPoints",
        label: {
            visible: true,
            format: "fixedPoint",
            precision: 0
        }
                    },
                    series: [
                        {valueField: "' . $valueField . '", name: "' . $valueName . '", color: "' . $colorData . '", opacity: .5},
                    ],
                    commonAxisSettings: {
                        label: {
                            visible: true
                        },
                        grid: {
                            visible: false,
                            color: "'.$colorAxis.'"
                        }
                    },
                    valueAxis: {
                        valueType: "numeric"
                    },
                    argumentAxis: {
                        type: "continuous",
                        inverted: true,
                        valueMarginsEnabled: false,
                        label: {
                            customizeText: function (arg) {
                                return arg.value;
                            }
                        },
                    },
                    legend: {
                         visible: false
                    }
                });
            });


            $(window).on("xenon.resize", function () {
                    $("#' . $layer . '").data("dxChart").render();

                });
        </script>
    ';


  echo '


       <div class="row">
            <div class="col-sm-3">
                <div class="xe-widget xe-progress-counter xe-progress-counter-info" xe-counter="" data-suffix="" style="background-color: '.$colorType.';"
                     data-count=".num" data-from="0" data-to="' . $dataUp . '" data-duration="4" data-easing="true">
                    <div class="xe-upper">

                        <div class="xe-icon">
                            <i class="linecons-user"></i>
                        </div>
                        <div class="xe-label">
                            <strong class="num">' . $dataUp . '</strong>
                            <span>' . $valueName . ': '.$tag2.'</span>
                        </div>

                    </div>
                    <div class="xe-lower">
                        <div class="border"></div>
                    </div>
                </div>';
if ($tag3<>''){
    echo '
                <div class="xe-widget xe-counter xe-counter-blue" xe-counter="" data-count=".num" data-from="1"
                     data-to="' . $dataButton . '" data-suffix="" data-duration="3" data-easing="true">
                    <div class="xe-icon">
                        <i class="linecons-user"></i>
                    </div>
                    <div class="xe-label">
                        <strong class="num">' . $dataButton . '</strong>
                        <span>' . $valueName . ': '.$tag3.'</span>
                    </div>
                </div>
                ';
            }

    echo '      </div>
            <div class="col-sm-9">


                <div class="panel panel-default">
                    <div class="panel-heading">
                        ' . $valueName . ': '.$tag1.'
                    </div>
                    <div class="panel-body">

                        <div class="row">
                            <div id="' . $layer . '" style="height: 180px;"></div> 
                        </div>

                    </div>
                </div>
            </div>
        </div>
';


}


function funnelGraph($data, $layer, $valueField, $title)
{
   // print_r(json_encode($data, JSON_NUMERIC_CHECK));

    echo '

		<script>


			$(function() {

                var dataSource' . $valueField . ' = JSON.parse(\'' . json_encode($data, JSON_NUMERIC_CHECK) . '\');



                var options = {
                    //width: 350,          // In pixels; defaults to containers width(if non - zero)
                    //height: 400,         // In pixels; defaults to containers height (if non-zero)
                    bottomWidth: 1 / 3,    // The percent of total width the bottom should be
                    bottomPinch: 0,      // How many sections to pinch
                    isCurved: true,     // Whether the funnel is curved
                    curveHeight: 20,     // The curvature amount
                    fillType: "solid",   // Either "solid" or "gradient"
                    isInverted: false,   // Whether the funnel is inverted
                    hoverEffects: true,  // Whether the funnel has effects on hover
                    dynamicArea: true   // Whether the funnel should calculate the blocks by
                                         // the count values rather than equal heights
                };


                var chart = new D3Funnel("#' . $layer . '");
                chart . draw(dataSource' . $valueField . ', options);

			});
        </script >



    <div class="panel panel-default" >
                    <div class="panel-heading" >
                        ' . $title . '
                    </div >
                    <div class="panel-body" >

                        <div class="row" >
                            Data:
                            <pre>'.print_r($data,true).'</pre>


                            <div id = "' . $layer . '" ></div >
                        </div >

                    </div >
                </div >

';
}


/**
 * @param $data
 * @param $layer
 * @param $valueField
 */
function wheelGraph($data, $layer, $valueField)
{

    print_r(json_encode($data, JSON_NUMERIC_CHECK));

    echo '
        <style >
@import("assets/js/d3/style.css");

            #circle circle {
                fill: none;
                pointer - events: all;
            }

            .group path {
    fill - opacity: .5;
            }

            path . chord {
    stroke: #000;
    stroke - width: .25px;
            }

            #circle:hover path.fade {
                display: none;
            }
        </style >


    <div class="panel panel-default" >
                    <div class="panel-heading" > Traffic Wheel </div >
                    <div class="panel-body" id = "wheel-1" >
                        <script >
                            var dataSource' . $valueField . ' = ' . json_encode($data, JSON_NUMERIC_CHECK) . ';
                            var width = $("#' . $layer . '") . width(),
                                    height = $("#' . $layer . '") . width(),
                                    outerRadius = Math . min(width, height) / 2 - 10,
                                    innerRadius = outerRadius - 24;

                            var formatPercent = d3 . format(".1%");

                            var arc = d3 . svg . arc()
    . innerRadius(innerRadius)
    . outerRadius(outerRadius);

                            var layout = d3 . layout . chord()
    . padding(.04)
    . sortSubgroups(d3 . descending)
    . sortChords(d3 . ascending);

                            var path = d3 . svg . chord()
    . radius(innerRadius);

                            var svg = d3 . select("#' . $layer . '") . append("svg")
    . attr("width", width)
    . attr("height", height)
    . append("g")
    . attr("id", "circle")
    . attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

                            svg . append("circle")
                            . attr("r", outerRadius);

                            var matrix = JSON . parse(dataSource' . $valueField . ' );

                            alert(matrix);

                            d3 . csv("nombrePrueba.csv", function (cities) {
                                d3 . json("matrizPrueba.json", function (matrix) {

                                    // Compute the chord layout.
                                    layout . matrix(matrix);

                                    // Add a group per neighborhood.
                                    var
                                    group = svg . selectAll(".group")
                                        . data(layout . groups)
                                        . enter() . append("g")
                                        . attr("class", "group")
                                        . on("mouseover", mouseover)
                                        . on("mouseout", mouseout);

                                    // Add a mouseover title.
                                    group . append("title") . text(function (d, i) {
                                        return cities[i] . name + ": " + d . value + "";
                                    });

                                    // Add the group arc.
                                    var
                                    groupPath = group . append("path")
                                        . attr("id", function (d, i) {
                                            return "group" + i;
                                        })
                                        . attr("d", arc)
                                        . style("fill", function (d, i) {
                                            return cities[i] . color;
                                            });

                                    // Add a text label.
                                    var
                                    groupText = group . append("text")
                                        . attr("x", 6)
                                        . attr("dy", 15);

                                    groupText . append("textPath")
                                    . attr("xlink:href", function (d, i) {
                                        return "#group" + i;
                                    })
                                    . text(function (d, i) {
                                        return cities[i] . name;
                                            });

                                    groupText . filter(function (d, i) {
                                        return groupPath[0][i] . getTotalLength() / 2 - 16 < this . getComputedTextLength();
                                    })
                                    . remove();

                                    // Add the chords.
                                    var
                                    chord = svg . selectAll(".chord")
                                        . data(layout . chords)
                                        . enter() . append("path")
                                        . attr("class", "chord")
                                        . style("fill", function (d) {
                                            return cities[d . source . index] . color;
                                            })
                                        . attr("d", path);

                                    // Add an elaborate mouseover title for each chord.
                                    chord . append("title") . text(function (d) {
                                        return cities[d . source . index] . name
                                        + " → " + cities[d . target . index] . name
                                        + ": " + d . source . value
                                        + "\n" + cities[d . target . index] . name
                                        + " → " + cities[d . source . index] . name
                                        + ": " + d . target . value;
                                    });

                                    function mouseover(d, i) {
                                        chord . classed("fade", function (p) {
                                            return p . source . index != i
                                            && p . target . index != i;
                                        });
                                    }

                                    function mouseout(d, i) {
                                        chord . classed("fade", function (p) {
                                            return p . source . index == i
                                            && p . target . index == i;
                                        });
                                    }

                                });
                            });

                        </script >


                    </div >
                </div >
';


}


/**
 * @param $ths
 * @param $data
 * @param $title
 * @param bool $printTotal
 * @param bool $checkToday
 * @param bool $totalData
 */



function barGauges(){

    echo '<script>
    $("#container").dxBarGauge({
    startValue: 0,
    endValue: 100,
    values: [100,200,300],
    label: {
        indent: 30,
        format: "fixedPoint",
        precision: 1,
        customizeText: function (arg) {
            return arg.valueText + ' %';
        }
    },
    title: {
        text: "Series Ratings",
        font: {
            size: 28
        }
    }
}); </script>';

    echo '<div id="container" class="containers" style="height: 440px; width: 100%;"></div>';

}



function printTableGeneral($ths, $data,$colorCol,$colorRow,$icon, $title, $printTotal = false, $checkToday = false, $totalData = false,$times=false,$now=false)
{
//print_r($times);
    echo '<div class="panel panel-default ng-scope" >
            <div class="panel-heading" >
                <h3 class="panel-title" > ' . $title . '</h3 >

                <div class="panel-options" >
                    <a href = "" data - toggle = "panel" >

                    ';
                    /*
                        echo '<label for="year">Choice date</label>
                        <select name="time" id="time">';
                        
                        if ($times!=false){
                            foreach ($times as $t) {

                                if ($now==$t['time']) {
                                    echo '<option value="'.$t['time'].'" selected>'.$t['time'].'</option>';
                                } else {
                                    echo '<option value="'.$t['time'].'" >'.$t['time'].'</option>';
                                }
                            }
                        }

                        echo '</select>
                        <input type="submit">
                    */


                    echo '    <span class="collapse-icon" ></span >
                        <span class="expand-icon" ></span >
                    </a >
                    <a href = "" data - toggle = "remove" >
    ×
                    </a >
                </div >
            </div >
            <div class="panel-body" >


                <div class="dataTables_wrapper form-inline dt-bootstrap" id = "example-1_wrapper" >

                    </div >
                    <div class="row" >

                        <table style = "width: 100%;" aria - describedby = "example-1_info" role = "grid" id = "example-1"
                           class="table table-striped table-bordered dataTable" cellspacing = "0" width = "100%" >
                        <thead >
                    <tr role = "row" >
    ';
// PAra cada columna pinto 
    count($ths);
    for ($i=0;$i<count($ths);$i++){

        
         echo '<th style="background-color:'.$colorCol['back'][$i]. '; color:'.$colorCol['text'][$i]. ';" "> ' . strtoupper($ths[$i]) . '</th > ';
        //echo '<th style="background-color="'.$colorCol['back'][$i].'";color="'.$colorCol['text'][$i].'"; " > ' . strtoupper($ths[$i]) . '</th > ';
    }

/*
    foreach ($ths as $th) {
        echo '<th > ' . strtoupper($th) . '</th > ';
    }
*/

    echo '
                  </tr >
                        </thead >

                        <tbody >

';

    $i = 0;
    foreach ($data as $row) {



        if ($i % 2 == 0) {
            echo '<tr class="odd" > ';


        } else {
            echo '<tr class="even" > ';
        }
        $j=0;
        foreach ($row as $column) {

            echo '<td style="background-color:"'.$colorRow['back'][$j].'";color:"'.$colorRow['text'][$j].'" "> ';

            if($j==0){
                    echo '<i class="'.$icon.'"></i>';
                }

             echo ''. $column . '</td > ';
            $j=$j+1;
        }


        echo '</tr > ';
        $i++;
    }
    echo '</tbody > ';
    if ($printTotal) {

            echo '<tfoot ><tr > ';
            $total = $totalData[0];
            $y = 0;

            // print_r($total);

            //var_dump($total);
            foreach ($total as $key => $th) {
                $key = str_replace('"','',$key);
                if ($y == 0) {
                    echo '<th> TOTAL</th> ';
                    echo '<td>'.$th . ' </td> ';
                } /*else if ($key == 'Total') {
                            echo '';
                        } */ else {
                    echo '<td>'.$th . ' </td> ';
                }
                $y++;
            }
            echo '</tr ></tfoot > ';

    }
    echo '

                    </table >
                    </div >
                </div >

            </div >

';


}






function barGraph_ok($data, $tag, $title,$subtitle, $layer, $valueField, $argumentField,$tipo)

{
    //print_r($data);
    echo '

            <script type="text/javascript">

                    var dataSource' . $tag . ' = ' . json_encode($data, JSON_NUMERIC_CHECK) . ';
                    jQuery(document).ready(function ($) {

                            $("#' . $layer . '").dxChart({
                            dataSource: dataSource'.$tag.',';


    $d= 'series: [';
        for($i=0;$i<count($valueField);$i++){
            if ($i>0){$d=$d. ',';}
            $d=$d.'{argumentField:"'.$argumentField.'",valueField:"'.$valueField[$i].'",name:"'.$valueField[$i].'",type:"'.$tipo.'"
,
        hoverMode: "allArgumentPoints",
        selectionMode: "allArgumentPoints",
        label: {
            visible: true,
            format: "fixedPoint",
            precision: 0
        }
        }';
        }
    $d=$d. '],

    palette: "Hard Pastel",
                                 title: "'.$subtitle.'",legend: {
        verticalAlignment: "bottom",
        horizontalAlignment: "bottom"
    },
                                 argumentAxis: { type: "discrete" }';

echo $d;


echo '                            });

                    });


                    $(window).on("xenon.resize", function () {
                        $("#' . $layer . '").data("dxChart").render();

                    });
            </script>

                <div class="panel panel-default"  >
                    <div class="panel-heading">
                        ' . $title . '
                    </div>
                    <div class="panel-body">
<!--
                                                Data:
                            <pre>'.print_r($data,true).'</pre>
-->                         
                        <div id="' . $layer . '"></div>
                    </div>
                </div>

    ';

}


function piramid3d($data){
    echo '        <script src="assets/amcharts/amcharts.js?1.0.0" type="text/javascript"></script>
        <script src="assets/amcharts/funnel.js" type="text/javascript"></script>
        <script type="text/javascript">

            var chart;
            var data = [';

            $datos='';
            foreach ($data as $key => $value) {
                if ($datos!=''){
                    $datos=$datos. '
                    ,{"title":"'.$key.'",
                    "value": '.$value.'}';
                } else {
                    $datos=$datos. '
                    {"title":"'.$key.'",
                    "value": '.$value.'}';
                }
            }

            echo $datos;
            echo '];';


            echo 'AmCharts.ready(function () {

                chart = new AmCharts.AmFunnelChart();
                chart.rotate = true;
                chart.titleField = "title";
                chart.balloon.fixedPosition = true;
                chart.marginRight = 210;
                chart.marginLeft = 15;
                chart.labelPosition = "right";
                chart.funnelAlpha = 0.9;
                chart.valueField = "value";
                chart.startX = -500;
                chart.dataProvider = data;
                chart.startAlpha = 0;
                chart.depth3D = 100;
                chart.angle = 30;
                chart.outlineAlpha = 1;
                chart.outlineThickness = 2;
                chart.outlineColor = "#FFFFFF";
                chart.write("chartdiv");
            });
        </script>



        <div id="chartdiv" style="width: 700px; height: 500px;"></div>';
}



function printPercent($text,$start,$end,$interval,$value,$lavel,$color,$width,$textSize){

    echo '
    <script type="text/javascript">

    jQuery(document).ready(function ($) {
        $("#'.$lavel.'").dxLinearGauge({
            scale: {
                startValue: '.$start.',
                endValue: '.$end.',
                majorTick: {
                    tickInterval: '.$interval.'
                },
                minorTick: {
                    visible: true,
                    tickInterval: '.$interval.'
                }
            },
            title: {
                text: "'.$text.'",
                font: { size: '.$textSize.' }
            },
            value: '.$value.'
        });
});
</script>
    ';

    echo '<div id="'.$lavel.'" class="containers" style=" background-color:'.$color.'; height: 100px; width:'.$width.';"></div>';
}


function barGaude($text,$start,$end,$base,$values,$unit,$size,$layer,$color,$height){
    echo '
    <script type="text/javascript">

        jQuery(document).ready(function ($) {
            $("#'.$layer.'").dxBarGauge({
            startValue: '.$start.',
            endValue: '.$end.',
            baseValue: '.$base.',
            values: [';
            $val='';
            for ($i=0;$i<count($values);$i++){
                if ($val==''){
                    $val=$val.$values[$i];
                } else {
                    $val=$val.','.$values[$i];
                }
            }
            echo $val.'
            ],
            label: {
                customizeText: function (arg) {
                    return arg.valueText + "'.$unit.'";
                }
            },
            palette: "ocean",
            title: {
                text: "'.$text.'",
                font: {
                    size: '.$size.'
                }
            }
        });
    });
    </script>';
    echo '<div id="'.$layer.'" class="containers" style="background-color:'.$color.' height: '.$height.'; width: 100%;"></div>';

}


function mapMarker(){

echo '

    <script type="text/javascript">

        jQuery(document).ready(function ($) {
var markerUrl = "http://js.devexpress.com/Demos/RealtorApp/images/map-marker.png";

var mapWidget = $("#map").dxMap({
    zoom: 14,
    height: 440,
    width: "100%",
    controls: true,
    markerIconSrc: markerUrl,
    markers: [{
            location: "Gran vía 17,Madrid,spain",
            tooltip: {
                text: "Gran via"
            }
        }, {
            location: "Goya 17,Madrid,spain",
            tooltip: {
                text: "Goya"
            }
        }, {
            location: "Calle de Adolfo Bioy Casares, 2,Madrid,spain",
            tooltip: {
                text: "Gavia"
            }
        }, {
            location: "Delicias 17,Madrid,spain",
            tooltip: {
                text: "Delicias"
            }
        }
    ]
}).dxMap("instance"); 

$("#useCustomMarkers").dxCheckBox({
    value: true,
    valueChangeAction: function(data) {
        mapWidget.option("markerIconSrc", data.value ? markerUrl : null);
    }
});

$("#showTooltips").dxCheckBox({
    value: true,
    valueChangeAction: function(data) {
        var newMarkers = $.map(mapWidget.option("markers"), function(item) {
            item.tooltip.isShown = data.value;
            return item;
        });

        mapWidget.option("markers", newMarkers);
    }
});

}
</script>

';

echo '
<div id="map"></div>

<div class="options">
    <div>       
        <div id="useCustomMarkers"></div>
        <div>Use custom marker icons</div>
    </div>
    <div>
        <div id="showTooltips"></div>
        <div>Show all tooltips</div>        
    </div>
</div>

';

}

?>


