<?php include ('graphs/graphs.php'); ?>
<?php include ('graphs/queries.php'); ?>
<?php


    $UserData=getUserCampaignData();


   $stores = array (
       'lagavia' => 'CC LA GAVIA',
       'granvia' => 'C GRAN VIA',
       'delicias' => 'PSO DELICIAS',
       'goya' => 'C GOYA'
   );

     $store = 'GENERAL';
     $filter = 'No';


/// Data capture




 $eventTable=getEventTable();
 $vInfo=getVisitorInfo();

$userResume=getUserResume();



 /*
$now='2015-01-01';

$dataHourly = getEvent($filter,$now);

$dataDaily = getTotal($filter);

$dataFinal = getTotalFinal($filter);

$donutData=getDataCircle($filter);

$thsHourly = array_keys($dataHourly[0]);

$thsDaily = array_keys($dataDaily[0]);

$dailyEvent=getPositionEvent($filter);
$dailyPromo=getPromotionEvent($filter);
$operator=getDataCircleForOperator($filter);

$times=getTimes();
$cp=getClienByCp($filter);
*/




//print_r($ths);


session_start();
/*
if ((!isset($_SESSION['user'])) || $_SESSION['user']=="" ){
    exit(-1);
}
*/
?>



<!DOCTYPE html>
<html lang="en">
<?php include('parts/header.php'); ?>
<body class="page-body">

<?php include ('parts/settings.php'); ?>

<!--
<script>
    $(function() {
$( "#datepicker" ).datepicker();

});
</script>
-->
<!--
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/js/date/query.datepick.css"> 
<script type="text/javascript" src="assets/js/date/jquery.plugin.js"></script> 
<script type="text/javascript" src="assets/js/date/jquery.datepick.js"></script>

<script>
$(function() {
$( "#campofecha" ).datepick({dateFormat: 'yyyy-mm-dd'});
});
</script>
-->
<!--
  <link rel="stylesheet" href="style.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.js"></script>
  <script src="app.js"></script>
-->

<link rel="stylesheet" type="text/css" href="pagination.css"> 



<div class="page-container">



    <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
    <!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
    <!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
    <?php include('parts/sidebar.php'); ?>

    <div class="main-content">


        <?php include ('parts/navbar.php'); ?>


        <!-- <input type="text" ng-model="date" datepicker /> -->

        <div ng-if="layoutOptions.pageTitles" title="Panels" description="Panels and their variants"
             class="page-title full-width ng-scope">
            <div class="title-env">
                <h1 class="title ng-binding">Campaigns </h1>
                
                <p class="description ng-binding"></p>


             <?php // print_r(getTrafficByShop()) ?> Overview by campaings

                <!-- <p>Date: <input type="text" id="campofecha"></p> -->
            </div>
        </div>

   
        <?php 



        lineGraph2('UserUnq',$userResume['data'], $userResume['total'],'',
          '#FF0000','#FF0000','#FF0000','#FF0000','#FF0000',
           'UNIQUE USERS DETAILS:', 'TOTAL UNIQUE USER',
           '','combo-layer-user',
           'user', 'Unique Users by zone', 'shop');
         ?>

        <!-- Usuarios por tipo -->

        <?php  

         printSquareRow($UserData);//printSquare($user); ?>

        <!-- Tabla de eventos por tienda -->
                <?php
         printTableGeneral($eventTable['columns'], $eventTable['data'],
          $eventTable['colorCol'],$eventTable['colorRow'],$eventTable['icon'],
          'Campaigns event for Shop/Type<br/>(Unique Users)', true,false, $eventTable['total'],false,false);
          ?>

          <!-- DETALLES POR TIENDA -->



          <?php 

          printLine('Details Campaigns by Shop'); 



          foreach ($stores as $key => $value) {

           
           printLineSub('<strong >DETAILS SHOP:</strong> '.$value); 

           $ud=getUserCampaignDataForShop($value); 

           
           $dataShop=getShopResume($value);
           if (1==1){
           $ds=array_reverse($dataShop['data']);
           lineGraph2(str_replace(' ', '', $key),$ds, $dataShop['total'],str_replace(' ', '', $key),
          '#00FF00','#00FF00','#00FF00','#FF0000','#FFFF00',
           $value, str_replace(' ', '', $key),
           str_replace(' ', '', $key),'layer-user-shop-'. str_replace(' ', '', $key),
           'users', 'UNIQUE USER BY TYPE ', 'Tipo');
         }

            printSquareRow($ud);

            $dataDailyShop = getEventDailyTable($value);
            $total[0]=$dataDailyShop['total'];


                    printTableGeneral($dataDailyShop['columns'], $dataDailyShop['data'],
          $dataDailyShop['colorCol'],$dataDailyShop['colorRow'],$dataDailyShop['icon'],
          'Campaigns event for Shop/Type<br/>(Unique Users)', true,true,$total,true,true);

         }


         ?>

        <!-- Usuarios por tipo -->


    </div>


 <?php include('parts/footer.php'); ?>

</body>
</html>