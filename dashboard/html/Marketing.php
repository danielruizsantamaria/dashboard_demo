<?php include ('graphs/graphs.php'); ?>
<?php include ('graphs/queries.php'); ?>
<?php




   $stores = array (
       'lagavia' => 'CC LA GAVIA',
       'granvia' => 'C GRAN VIA',
       'delicias' => 'PSO DELICIAS',
       'goya' => 'C GOYA'
   );


 if (isset($_REQUEST['store'])) {
     $store = $stores[$_REQUEST['store']];
     $filter = $store; 





 } else {
     $store = 'GENERAL';
     $filter = 'No';
 }


/// Data capture

$dataAnonimus=getDonutDataForUserAnonimus();
$vInfo=getVisitorInfo();






session_start();
/*
if ((!isset($_SESSION['user'])) || $_SESSION['user']=="" ){
    exit(-1);
}
*/
?>



<!DOCTYPE html>
<html lang="en">
<?php include('parts/header.php'); ?>
<body class="page-body">

<?php include ('parts/settings.php'); ?>

<!--
<script>
    $(function() {
$( "#datepicker" ).datepicker();

});
</script>
-->
<!--
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/js/date/query.datepick.css"> 
<script type="text/javascript" src="assets/js/date/jquery.plugin.js"></script> 
<script type="text/javascript" src="assets/js/date/jquery.datepick.js"></script>

<script>
$(function() {
$( "#campofecha" ).datepick({dateFormat: 'yyyy-mm-dd'});
});
</script>
-->
<!--
  <link rel="stylesheet" href="style.css">
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.5/angular.js"></script>
  <script src="app.js"></script>
-->

<link rel="stylesheet" type="text/css" href="pagination.css"> 



<div class="page-container">



    <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
    <!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
    <!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
    <?php include('parts/sidebar.php'); ?>

    <div class="main-content">


        <?php include ('parts/navbar.php'); ?>


        <!-- <input type="text" ng-model="date" datepicker /> -->

        <div ng-if="layoutOptions.pageTitles" title="Panels" description="Panels and their variants"
             class="page-title full-width ng-scope">
            <div class="title-env">
                <h1 class="title ng-binding">CRM & MARKETING  </h1>
                
                <p class="description ng-binding"></p>


             <?php // print_r(getTrafficByShop()) ?> Overview by type of users

                <!-- <p>Date: <input type="text" id="campofecha"></p> -->
            </div>
        </div>


      <?php printLine('ANONYMOUS USERS'); ?>


        <?php  $user=getUserData();unset($user[3]);printSquareRow($user); ?>



          <div class="row">
              <?php donutGraph($dataAnonimus['data'],$dataAnonimus['title'],'traffic-layer-2','Percent3', 'Shop'); ?>           
          </div>


        <?php  $vInfo=getVisitorInfo();printSquareRow($vInfo) ?>


        <?php //$data=array('Web'=>1,'otro'=>2,'mas'=>3); piramid3d($data);?>


          <div class="row">
              <?php $dataVisitor=getDonutVisitorDetail();donutGraph($dataVisitor['data'],$dataVisitor['title'],'visitor-layer-2-vistor','Percent2', 'Shop'); ?>           
          </div>


          <div class="row">
            <div class="col-sm-6">
              <?php accelGraph('layer-new-visit-clock',0, 100, 5, 'Ratio', 87, 'New visits'); ?>
            </div>  
            <div class="col-sm-6">
              <?php accelGraph('layer-average-clock',16, 18, 0.2, 'Average time', 17.1, 'Time for visit'); ?>
            </div>              
          </div>

          
        <?php 
        $vf=getVisitFrecuency();
        barGraph_ok($vf,'datosAA','Users by number of visits','Number of visits',
         'number-visits-layer', ['Users']
         , 'Visits','bar');  

         ?>



        <?php 
        $tv=getAverageTimeForVisit();
        barGraph_ok($tv,'datosAAA','Time for visit','Average Time',
         'time-visits-layer', ['Time']
         , 'Visits','bar');  

         ?>

        <?php 
        $at=getDonutAverageTime();

          echo '<div class="row">';
               donutGraph($at['data'],$at['title'],'avcerage-time-layer-3','Percent10', 'time');         
          echo '</div>';  

         ?>




  <?php printLine('NAMED USERS'); ?>


      <?php  $namedUser=getNamedUsers();printSquareRow($namedUser) ?>


         <div class="row">
              <?php $activeUser=getDonutActiveUser();donutGraph($activeUser['data'],$activeUser['title'],'Active-user-layer-2','Percent4', 'Shop'); ?>           
          </div>

         <div class="row">
              <?php $zipCode=getDonutZipCode();donutGraph($zipCode['data'],$zipCode['title'],'Zip-user-layer-2','Percent6', 'zone'); ?>           
          </div>

         <div class="row">
              <?php $so=getDonutSO();donutGraph($so['data'],$so['title'],'SO-user-layer-2','Percent7', 'device'); ?>           
          </div>

         <div class="row">
              <?php $mmo=getDonutMMO();donutGraph($mmo['data'],$mmo['title'],'mmo-user-layer-2','Percent8', 'mmo'); ?>           
          </div>


  <?php printLine('Users surveys'); ?>



    <?php

      printLineSub('¿Como nos has conocido?'); 
            echo '<div class="row" style=" background-color:#ffffff; ">';
      printPercent('Me entere por la propia App',0,100,20,18.75,'r1','#ffffff','70%',15);
      printPercent('Me he enterado en la tienda',0,100,20,12.50,'r2','#ffffff','70%',15);
      printPercent('Mi movil me aviso',0,100,20,56.25,'r3','#ffffff','70%',15);
      printPercent('Otros',0,100,20,12.50,'r4','#ffffff','70%',15);
      //printPercent('Me entere por la propia App',0,100,5,40,'e2');
    ?>
</div>



    <?php

      printLineSub('¿Habrías venido si no conocieses la promoción?'); 
            echo '<div class="row" style=" background-color:#ffffff; ">';
      printPercent('He venido por causa de la promoción',0,100,20,31.25,'rr1','#ffffff','70%',15);
      printPercent('Lo más probable es que no',0,100,20,25.00,'rr2','#ffffff','70%',15);
      printPercent('Si, tenia pensado venir',0,100,20,56.25,'rr3','#ffffff','70%',15);
      //printPercent('Me entere por la propia App',0,100,5,40,'e2');
    ?>
</div>


    <?php

      printLineSub('¿Que funcionalidades te gustaía poder realizar mediante la App?'); 
            echo '<div class="row" style=" background-color:#ffffff; ">';
      printPercent('Consultar Stock de móviles',0,100,20,6.25,'rrr1','#ffffff','70%',15);
      printPercent('Pedir turno',0,100,20,6.25,'rrr2','#ffffff','70%',15);
      printPercent('Poder pagar con mi móvil',0,100,20,12.50,'rrr3','#ffffff','70%',15);
      printPercent('Promociones exclusivas',0,100,20,43.75,'rrr4','#ffffff','70%',15);
      printPercent('Recibir ofertas en mi móvil',0,100,20,31.25,'rrr5','#ffffff','70%',15);
      //printPercent('Me entere por la propia App',0,100,5,40,'e2');
    ?>
</div>



    <?php

      printLineSub('¿Cuan es tu valoración de la App en una escala del 1 al 5?'); 

      echo '<div class="row" style=" background-color:#ffffff; ">';
      printPercent('Valoración de 3',0,100,20,12.50,'rrrr1','#ffffff','70%',15);
      printPercent('Valoración de 4',0,100,20,18.75,'rrrr2','#ffffff','70%',15);
      printPercent('Valoración de 5',0,100,20,68.75,'rrrr3','#ffffff','70%',15);
      //printPercent('Me entere por la propia App',0,100,5,40,'e2');
    ?>
</div>


  <?php
    echo '<div class="row" style="background-color:#ffffff;padding:100px">';
   barGaude(-50,50,0,[-21.3, 14.8, -30.9, 45.2],28,'capa-test-23','#ffffff','450px');
   echo '</div>';
  ?>




    </div>


 <?php include('parts/footer.php'); ?>

</body>
</html>