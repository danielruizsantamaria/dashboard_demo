<?php
/**
 * Created by PhpStorm.
 * User: aritoru
 * Date: 12/22/14
 * Time: 12:00 PM
 */
?>


<ul id="main-menu" class="main-menu">
                <!-- add class "multiple-expanded" to allow multiple submenus to open -->
                <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
                <li class="opened active">
                    <a href="general.php">
                        <i class="linecons-user"></i>
                        <span class="title">Dashboard data</span>
                    </a>
                    <ul>
                        <li>
                            <a href="activity.php">
                                <span class="title">Activity & Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="Campaigns.php">
                                <span class="title">Campaigns</span>
                            </a>
                        </li>
                        <li>
                            <a href="Marketing.php">
                                <span class="title">CRM & Marketing</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>